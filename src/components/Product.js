import { Card, Col, Button } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2';


export default function Product({ productProp }){

	let adminUser = localStorage.getItem("isAdmin");
	const navigate = useNavigate();
	const { _id, name, description, price, stock, isAvailable } = productProp;
	let token = localStorage.getItem("token");


	const deleteProduct = (e) => {
		e.preventDefault()
		if(!adminUser){
			Swal.fire({
				title: 'Deletion failed',
				icon: 'error',
				text: 'You need to login first to delete a product'
			})
			navigate("/login", { replace: true });
		}else{
			fetch(`https://murmuring-hamlet-18839.herokuapp.com/products/${_id}`,{
				method: "DELETE",
				headers:{
					Authorization: `Bearer ${token}`,
				}
			})
			.then(res=> res.json())
			.then(data => {
				if(data===true){
					Swal.fire({
						title: "Deletion successful",
						icon: 'success',
						text: 'You have successfully Deleted.'
					})
				}else{
					Swal.fire({
						title: "Deletion failed",
						icon: 'error',
						text: 'You have not deleted a product.'
					})
				}
			})
		}
	}

	const rightButton = (adminUser)?(
		<>	
			<div className="text-right">
				<Link to={`/editproduct/${_id}`}><Button variant="success" className="m-2">Edit</Button></Link>
				<Button variant="success" onClick={e => deleteProduct(e)}>Delete</Button>
			</div>
		</>
	):(
		<>	
			<div className="text-right">
				<Link to={`/cart`}><Button variant="success" className="m-2">Add</Button></Link>
				<Link to={`/products/${_id}`}><Button variant="success" className="m-2">Detail</Button></Link>
			</div>
		</>
	);

	const availability = (isAvailable)?(
		<>	
			<p className="my-0">
				Status: Available
			</p>
		</>
	):(
		<>	
			<p className="mb-0">
				Status: Not Available
			</p>
		</>
	);


	return (
		<Col xs={6} md={4} lg={3} className="my-3">
			<Card className="my-3 mx-auto h-100">
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Text>

						<h5>Description:</h5>
						<p> {description} </p>

						<p>Price: USD {price}</p>
						{availability}
						<p>Stock: {stock}</p>
						
					</Card.Text>
				</Card.Body>
				<Card.Footer>
					{ rightButton }					
				</Card.Footer>
			</Card>
		</Col>
	)

}
