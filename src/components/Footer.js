import { Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Footer(){

	return(
		<Container fluid className="bg-dark mt-5 footer">
				<div class="text-white py-3 d-flex flex-row justify-space-between">

					<div class="flex-column ml-3 mr-5">
						<Link to="/" className="text-white"><div>Home</div></Link>
					    <Link to="/products" className="text-white"><div>Products</div></Link>
					    <Link to="/contact" className="text-white"><div>Contact</div></Link>
					</div>

					<div className="flex-column">
						<div>Follow Me</div>
			        	<div>Instagram</div>
			            <div>Facebook</div>
					</div>

				</div>
				<div className="text-white text-weight-light text-center pb-3">
					Copyright &copy; My Website 2022
				</div>
		</Container>
	)
}