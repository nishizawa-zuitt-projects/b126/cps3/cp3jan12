import { useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import "./App.css"

import UserContext from './UserContext';

import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';

import AddProduct from './pages/AddProduct';
import EditProduct from './pages/EditProduct';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import Products from './pages/Products';
import Profile from './pages/Profile';
import Register from './pages/Register';


export default function App() {

    const [user, setUser] = useState({ 
        token: localStorage.getItem('token')
    });

    const unsetUser = () => {
        localStorage.clear();
        setUser({ token: null});
    }

    return (
        <UserContext.Provider value={{ user, setUser, unsetUser }}>
            <BrowserRouter>
                <AppNavbar/>
                <Routes>
                    <Route exact path="/" element={<Home/>}/>
                    <Route exact path="/login" element={<Login/>}/>
                    <Route exact path="/logout" element={<Logout/>}/>
                    <Route exact path="/register" element={<Register/>}/>
                    <Route path="*" element={<NotFound/>}/>
                    <Route exact path="/profile" element={<Profile/>}/>
                    <Route exact path="/products" element={<Products/>}/>
{/*                    <Route exact path="/products/:productID" element={<Product/>}/>*/}
                    <Route exact path="/editproduct/:id" element={<EditProduct/>}/>  
                    <Route exact path="/addproduct" element={<AddProduct/>}/>                   
                </Routes>
                <Footer/>
            </BrowserRouter>
        </UserContext.Provider>
    );
}