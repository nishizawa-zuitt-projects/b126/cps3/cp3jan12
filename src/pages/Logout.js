import { useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { useNavigate  } from 'react-router-dom'



export default function Logout(){
	const { unsetUser, setUser } = useContext(UserContext);
	const navigate = useNavigate();
	unsetUser();

	useEffect(() => {
		setUser({id: null});
	})

	return(
		navigate("/login", { replace: true })
	)
}