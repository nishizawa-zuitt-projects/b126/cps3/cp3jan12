import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import { Row, Container, Button } from 'react-bootstrap';
import Product from "../components/Product";

export default function Products(){

	let adminUser = localStorage.getItem("isAdmin");

	const [productsData, setProductsData] = useState([])

	useEffect(() => {

		fetch('https://murmuring-hamlet-18839.herokuapp.com/products/')
		.then(res => res.json())
		.then(data => {
			setProductsData(data)
		})

	}, [])

	const products = productsData.map(product => {
		return(
			<Product key={product._id} productProp={product}/>
		)
	})

	const addProduct = (adminUser)?(
			<Link to="/addproduct"><Button variant="success" className="mx-2 text-right">Add Product</Button></Link>
	):(
		<>	
		</>
	);

	return(
		<>
			<Container className="my-5">
				<div className="mt-5 mb-3">
					<h4 className="text-center">Products { addProduct }</h4>
				</div>
				<Row>
					{products}
				</Row>
			</Container>
		</>
	)
}