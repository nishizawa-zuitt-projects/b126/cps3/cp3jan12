import { Link } from 'react-router-dom';

import Container from 'react-bootstrap/Container';

export default function NotFound() {
	return (
		<Container className="mt-3 text-center">
			<h3>Page Not Found</h3>
			<p>Go back to the <Link to="/">homepage</Link>.</p>
		</Container>
	);
}