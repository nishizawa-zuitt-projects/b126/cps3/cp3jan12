import { useState, useEffect } from 'react';
import { Container, Form, Button, Col } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2';

export default function Register(){

	const [firstName, setFirstName] =  useState("")
	const [lastName, setLastName] =  useState("")
	const [email, setEmail] =  useState("")
	const [mobileNo, setMobileNo] =  useState("")
	const [street, setStreet] =  useState("")
	const [city, setCity] =  useState("")
	const [state, setState] =  useState("")
	const [zipCode, setZipCode] =  useState("")
	const [password1, setPassword1] =  useState("")
	const [password2, setPassword2] =  useState("")
	const [isActive, setIsActive] = useState(false)
	const navigate = useNavigate();


	useEffect(() => {
		if((firstName !== "" && lastName !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [firstName, lastName, email, mobileNo, street, city, state, zipCode, password1, password2])

	const registerUser = (e) => {
		e.preventDefault()

		fetch('https://murmuring-hamlet-18839.herokuapp.com/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Duplicate email found",
					icon: 'error',
					text: "Please provide a different email"
				})
			}else{
				fetch('https://murmuring-hamlet-18839.herokuapp.com/register', {
					method: 'POST',
					
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo,
						address: {
							street: street,
							city: city,
							state: state,
							zipCode: zipCode
						}
					})
				})
				.then(res => res.json())
				.then(data => {

					if(data){
						Swal.fire({
							title: "Registration successful",
							icon: 'success',
							text: 'You have been successfully registerd. Please login.'
						})

						setFirstName("")
						setLastName("")
						setEmail("")
						setMobileNo("")
						setStreet("")
						setCity("")
						setState("")
						setZipCode("")
						setPassword1("")
						setPassword2("")
					}else{
						Swal.fire({
							title: "Registration failed",
							icon: 'error',
							text: 'You have not been registerd'
						})
					}
				navigate("/login", { replace: true });
				})
			}
		})		
	}

	return(
		<Container className="my-3">
			<h3 className="my-3 text-center">Registration</h3>
			<Form className="my-3" onSubmit={e => registerUser(e)}>
				<Form.Row>
					<Form.Group as={Col} controlId="firstName">
						<Form.Label>First Name</Form.Label>
						<Form.Control type="text" placeholder="Enter your first name" required value={firstName} onChange={e => setFirstName(e.target.value)}/>
					</Form.Group>

					<Form.Group as={Col} controlId="lastName">
						<Form.Label>Last Name</Form.Label>
						<Form.Control type="text" placeholder="Enter your last name" required value={lastName} onChange={e => setLastName(e.target.value)}/>
					</Form.Group>
				</Form.Row>

				<Form.Row>
					<Form.Group as={Col} controlId="email">
						<Form.Label>Email Address</Form.Label>
						<Form.Control type="email" placeholder="Enter your email" required value={email} onChange={e => setEmail(e.target.value)}/>
					</Form.Group>

					<Form.Group as={Col} controlId="mobileNo">
						<Form.Label>Mobile Number</Form.Label>
						<Form.Control type="text" placeholder="Enter your mobile number" required value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
					</Form.Group>
				</Form.Row>

				<Form.Group controlId="addressStreet">
					<Form.Label>Street</Form.Label>
					<Form.Control type="text" placeholder="Apartment, studio, or floor" required value={street} onChange={e => setStreet(e.target.value)} />
				</Form.Group>

				<Form.Row>
					<Form.Group as={Col} controlId="addressCity">
						<Form.Label>City</Form.Label>
						<Form.Control type="text" placeholder="City" required value={city} onChange={e => setCity(e.target.value)}/>
					</Form.Group>

					<Form.Group as={Col} controlId="addressState">
						<Form.Label>State</Form.Label>
						<Form.Control type="text" placeholder="State" required value={state} onChange={e => setState(e.target.value)}/>
					</Form.Group>

					<Form.Group as={Col} controlId="addressZipCode">
						<Form.Label>Zip Code</Form.Label>
						<Form.Control type="text" placeholder="zipCode" required value={zipCode} onChange={e => setZipCode(e.target.value)}/>
					</Form.Group>
				</Form.Row>

				<Form.Row>
					<Form.Group as={Col} controlId="password1">
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Enter your password" required value={password1} onChange={e => setPassword1(e.target.value)}/>
					</Form.Group>

					<Form.Group as={Col} controlId="password2">
						<Form.Label>Verify Password</Form.Label>
						<Form.Control type="password" placeholder="Verify your password" required value={password2} onChange={e => setPassword2(e.target.value)}/>
					</Form.Group>
				</Form.Row>

				{(isActive)
					?
					<Button variant="success" type="submit">Submit</Button>
					:
					<Button variant="success" disabled>Submit</Button>
				}
			</Form>
		</Container>
	)
}