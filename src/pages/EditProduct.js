import { useState, useEffect } from 'react';
import { Container, Form, Button, Col } from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom'
import Swal from 'sweetalert2';

export default function EditProduct(){

	const [name, setName] =  useState("");
	const [productCategory, setProductCategory] =  useState("");
	const [price, setPrice] =  useState("");
	const [stock, setStock] =  useState("");
	const [isAvailable, setIsAvailable] =  useState("");
	const [description, setDescription] =  useState("");
	const [isActive, setIsActive] = useState(false);
	const navigate = useNavigate();
	

	useEffect(() => {
		if(name !== "" && productCategory !== "" && price !== "" && stock !== "" && description !== "" && isAvailable !== ""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [name, productCategory, price, stock,isAvailable, description])

	const editingProduct = (e) => {
		e.preventDefault()
		let token = localStorage.getItem("token");
		let adminUser = localStorage.getItem("isAdmin");
		const params = useParams

		if(adminUser){
			fetch(`https://murmuring-hamlet-18839.herokuapp.com/products/${params}`, {
				method: 'PUT',
				headers: {
					Authorization: `Bearer ${token}`,
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					name: name,
					productCategory: productCategory,
					price: price,
					stock: stock,
					description: description,
				})
			})
			.then(res => res.json())
			.then(data => {

				if(data){
					Swal.fire({
						title: "Adding a product successful",
						icon: 'success',
						text: 'You have successfully added a product.'
					})
					setName("")
					setProductCategory("")
					setPrice("")
					setDescription("")

				}else{
					Swal.fire({
						title: "Registration failed",
						icon: 'error',
						text: 'You have not been registerd'
					})
				}
			
			})
		}else{
			navigate("/products", { replace: true });
		}	
	navigate("/products", { replace: true });
	}

	return(
		<Container className="my-3">
			<h3 className="my-3 text-center">Edit Product</h3>
			<Form className="my-3" onSubmit={e => editingProduct(e)}>
				<Form.Group controlId="name">
					<Form.Label>Product Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter a product name" required value={name} onChange={e => setName(e.target.value)}/>
				</Form.Group>
				<Form.Row>
					<Form.Group as={Col} controlId="price">
						<Form.Label>Price:</Form.Label>
						<Form.Control type="number" placeholder="Enter a price" required value={price} onChange={e => setPrice(e.target.value)}/>
					</Form.Group>

					<Form.Group as={Col} controlId="stock">
						<Form.Label>Stock:</Form.Label>
						<Form.Control type="number" placeholder="Enter a number of stock" required value={stock} onChange={e => setStock(e.target.value)}/>
					</Form.Group>

					<Form.Group as={Col} controlId="status">
						<Form.Label>Status:</Form.Label>
						<Form.Control as="select" defaultValue="choose.." required value={isAvailable} onChange={e => setIsAvailable(e.target.value)}>
						<option>Choose...</option>
						<option>true</option>
						<option>false</option>
						</Form.Control>
					</Form.Group>

					<Form.Group as={Col} controlId="productCategory">
						<Form.Label>Product Category:</Form.Label>
						<Form.Control as="select" defaultValue="choose.." required value={productCategory} onChange={e => setProductCategory(e.target.value)}>
						<option>Choose...</option>
						<option>Close-up</option>
						<option>Stage</option>
						<option>Accessory</option>
						<option>Lecture</option>
						</Form.Control>
					</Form.Group>
				</Form.Row>
				<Form.Group controlId="productDescription">
				  <Form.Label>Description:</Form.Label>
				  <Form.Control as="textarea" rows={3} required value={description} onChange={e => setDescription(e.target.value)}/>
				</Form.Group>

				{(isActive)
					?
					<div className="text-right">
						<Button variant="success" type="submit">Add</Button>
					</div>
					:
					<div className="text-right">
						<Button variant="success" disabled>Add</Button>
					</div>
				}
			</Form>
		</Container>
	)
}