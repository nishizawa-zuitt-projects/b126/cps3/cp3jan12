import { useState } from 'react';
import { Container, Button, Col, Row } from 'react-bootstrap';

export default function Profile (){

	const [userData, setUserData] = useState()

	let token = localStorage.getItem("token");

	if(token !==""){
		fetch('https://murmuring-hamlet-18839.herokuapp.com/users/details',{
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setUserData(data)
		})
	}else{
		return null
	}

	return (
		<Container fluid>
			<div>
				<h3>Profile</h3>
			</div>
			<Row>
				<Col>
					<div>
						<h4>User Name: {userData.firstName} {userData.lastName}</h4>
						<p>Email Address: {userData.email}</p>
						<p>Mobile Number: {userData.mobileNo}</p>
						<p>Address: {userData.address}</p>
						<Button>Edit</Button>
					</div>
				</Col>
			</Row>
		</Container>
	)
}