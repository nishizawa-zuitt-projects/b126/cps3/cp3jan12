import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { useNavigate  } from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isLoginDisabled, setIsLoginDisabled] = useState(false);
    const navigate = useNavigate();
    const {user, setUser} = useContext(UserContext);

    useEffect(() => {
        if (email === '' || password === '') {
            setIsLoginDisabled(true);
        } else {
            setIsLoginDisabled(false);
        }

    }, [email, password]);

    const login = async (e) => {
        e.preventDefault();

        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email, password })
        };

        const response = await fetch('https://murmuring-hamlet-18839.herokuapp.com/users/login', payload);
        const data = await response.json();

        if (typeof data.access !== 'undefined') {
            localStorage.setItem('token', data.access);
            getUserData(data.access);
            Swal.fire('Login successfull','You have been successfully Loggedin','success')
            setEmail("")
            setPassword("")
            setUser("");
            navigate("/", { replace: true });

        } else {
            Swal.fire('Login Failed', 'Credentials not found, try again.', 'error');
        }
    }

    const getUserData = async (token) => {
        const payload = {
            headers: {
                'Authorization': `Bearer ${token}`
            }
        };

        const response = await fetch('https://murmuring-hamlet-18839.herokuapp.com/users/details', payload);
        const data = await response.json();
        localStorage.setItem('id', data._id);
        localStorage.setItem('isAdmin', data.isAdmin);
    }

    return (

        <Container>
            <h3 className="m-3">Login</h3>
            <Form className="m-3" onSubmit={login}>
                <Form.Group>
                    <Form.Label>Email</Form.Label>
                    <Form.Control type="text" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Enter password" value={password} onChange={e => setPassword(e.target.value)} required/>
                </Form.Group>
                <Button type="submit" variant="success" disabled={isLoginDisabled}>Login</Button>
            </Form>
        </Container>
    );
}